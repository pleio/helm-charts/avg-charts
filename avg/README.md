# avg

![Version: 0.1.2](https://img.shields.io/badge/Version-0.1.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square)

A Helm chart for deploying AVG

## Source Code

* <https://gitlab.com/pleio/helm-charts/avg-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| config.allowedhost | string | `nil` |  |
| config.database.host | string | `nil` |  |
| config.database.name | string | `nil` |  |
| config.database.password | string | `nil` |  |
| config.database.username | string | `nil` |  |
| config.email.from | string | `nil` |  |
| config.email.host | string | `nil` |  |
| config.email.password | string | `nil` |  |
| config.email.port | string | `nil` |  |
| config.email.user | string | `nil` |  |
| config.oauth.clientId | string | `nil` |  |
| config.oauth.clientSecret | string | `nil` |  |
| config.oauth.url | string | `nil` |  |
| config.secretkey | string | `nil` |  |
| domain | string | `"avg.foobar.tld"` |  |
| existingSecret | string | `nil` | Refer to an existing secret to avoid managing secrets through Helm. |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"registry.gitlab.com/pleio/avg"` |  |
| image.tag | string | `"latest"` |  |
| rollingUpdate.maxSurge | int | `1` |  |
| rollingUpdate.maxUnavailable | int | `0` |  |
| service.port | int | `80` |  |
| service.type | string | `"ClusterIP"` |  |
| useLetsEncryptCertificate | bool | `false` | A boolean value indicating whether to use an automatically obtained LetsEncrypt certificate for TLS. |
| wildcardTlsSecretName | string | `""` | The name of the K8S secret containing the key pair for the wildcard certificate used. To obtain a LetsEncrypt certificate, set to a name not associated to any secret to allow cert-manager to request a certificate. |
